# # **ISTN 전자세금계산서 HTTP Sample**
![](https://img.shields.io/badge/version-0.0.1-green)




## **Git 링크**
[https://gitlab.com/weaver7/node-taxinvoice-sample.git](https://gitlab.com/weaver7/node-taxinvoice-sample.git)

## **설치순서**

### **환경**
| SDK    | Version |
| :----- | :------ |
| NodeJS | 12.18.4 |

### **1. 터미널을 통해 git clone 또는 다운로드**

```
> git clone https://gitlab.com/weaver7/node-taxinvoice-sample.git
```

### **2. npm install**
프로젝트 Root에서 터미널에 명령어 입력
```bash
> npm install
```

### **3. Server Start**
프로젝트 Root에서 터미널에 명령어 입력
```bash
> npm start
```
정상적으로 실행시 아래와 같이 출력
```bash
PS \node-taxinvoice-apps-sample> npm start

> node-taxinvoice-apps-sample@1.0.0 start \node-taxinvoice-apps-sample
> node .

Server dev 5200
```






---
## **API 테스트**




### **1. 전자세금계산서 발행 (TAX_C001) - GOT / Request Params**

Request Param을 이용해 전자세금계산서 발행 (GOT)

```http
POST http://localhost:5200/api/v1/taxinvoice/post_got_param
```




### **2. 전자세금계산서 발행 (TAX_C001) - GOT / Request Body**

Reqeust Body을 이용해 전자세금계산서 발행 (GOT)

```http
POST http://localhost:5200/api/v1/taxinvoice/post_got_body
```




### **3. 전자세금계산서 발행 (TAX_C001) - Axios / Request Params**

Request Params을 이용해 전자세금계산서 발행 (Axios)

```http
POST http://localhost:5200/api/v1/taxinvoice/post_axios_param
```




### **4. 전자세금계산서 발행 (TAX_C001) - Axios / Request Body**

Request Body을 이용해 전자세금계산서 발행 (Axios)

```http
POST http://localhost:5200/api/v1/taxinvoice/post_axios_body
```
