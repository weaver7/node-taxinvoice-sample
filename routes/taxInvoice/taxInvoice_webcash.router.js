'use strict';
const router = require('express').Router();
const { taxInvoiceWebcashController } = require('../../controllers');


/**
 * * 001_전자세금계산서 발행 (TAX_C001) - GOT Module (Request Params 사용)
 */
router.post(
  '/post_got_param',
  taxInvoiceWebcashController.procPostTaxInvoiceGotRequestParams
);

/**
 * * 002_전자세금계산서 발행 (TAX_C001) - GOT Module (Request Body 사용)
 */
 router.post(
  '/post_got_body',
  taxInvoiceWebcashController.procPostTaxInvoiceGotRequestBody
);

/**
 * * 003_전자세금계산서 발행 (TAX_C001) - Axios Module (Request Params 사용)
 */
 router.post(
  '/post_axios_param',
  taxInvoiceWebcashController.procPostTaxInvoiceAxiosRequestParams
);

/**
 * * 004_전자세금계산서 발행 (TAX_C001) - Axios Module (Request Body 사용)
 */
 router.post(
  '/post_axios_body',
  taxInvoiceWebcashController.procPostTaxInvoiceAxioisRequestBody
);


module.exports = router;