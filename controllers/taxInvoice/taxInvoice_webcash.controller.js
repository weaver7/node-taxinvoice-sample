const commonUtils = require('../../utils/Common');
const { taxInvoiceWebcashService } = require('../../services');

/**
 * * 001_전자세금계산서 발행 (TAX_C001) - GOT Module (Request Params 사용)
 * @param {*} req
 * @param {*} res
 */
exports.procPostTaxInvoiceGotRequestParams = async (req, res) => {
  try {
    const result = await taxInvoiceWebcashService.procPostTaxInvoiceGotRequestParams({
      request_data: req.body,
      headers: req.headers
    });
    if (result instanceof Error) throw result;
    res.status(200).send(result);
  } catch (error) {
    return res.status(error.status || 500).send(commonUtils.printError(error));
  }
};


/**
 * * 002_전자세금계산서 발행 (TAX_C001) - GOT Module (Request Body 사용)
 * @param {*} req
 * @param {*} res
 */
 exports.procPostTaxInvoiceGotRequestBody = async (req, res) => {
  try {
    const result = await taxInvoiceWebcashService.procPostTaxInvoiceGotRequestBody({
      request_data: req.body,
      headers: req.headers
    });
    if (result instanceof Error) throw result;
    res.status(200).send(result);
  } catch (error) {
    return res.status(error.status || 500).send(commonUtils.printError(error));
  }
};


/**
 * * 003_전자세금계산서 발행 (TAX_C001) - Axios Module (Request Params 사용)
 * @param {*} req
 * @param {*} res
 */
 exports.procPostTaxInvoiceAxiosRequestParams = async (req, res) => {
  try {
    const result = await taxInvoiceWebcashService.procPostTaxInvoiceAxiosRequestParams({
      request_data: req.body,
      headers: req.headers
    });
    if (result instanceof Error) throw result;
    res.status(200).send(result);
  } catch (error) {
    return res.status(error.status || 500).send(commonUtils.printError(error));
  }
};


/**
 * * 004_전자세금계산서 발행 (TAX_C001) - Axios Module (Request Body 사용)
 * @param {*} req
 * @param {*} res
 */
 exports.procPostTaxInvoiceAxioisRequestBody = async (req, res) => {
  try {
    const result = await taxInvoiceWebcashService.procPostTaxInvoiceAxioisRequestBody({
      request_data: req.body,
      headers: req.headers
    });
    if (result instanceof Error) throw result;
    res.status(200).send(result);
  } catch (error) {
    return res.status(error.status || 500).send(commonUtils.printError(error));
  }
};