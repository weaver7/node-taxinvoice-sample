const dayjs = require('dayjs');
const utc = require('dayjs/plugin/utc');
const timezone = require('dayjs/plugin/timezone');

/**
 * * Cloud Environments
 * @returns 
 */
exports.getEnvPath = () => {
  const { isLocal, app } = cfenv.getAppEnv();
  if (isLocal) return path.join("envs", "lcl.env");
  const { cf_api, space_name } = app;
  switch (true) {
    case cf_api === "https://api.cf.jp10.hana.ondemand.com":
      return path.join("envs", `jp10.${space_name}.env`);

    case cf_api === "https://api.cf.jp20.hana.ondemand.com":
      return path.join("envs", `jp20.${space_name}.env`);

    case cf_api === "https://api.cf.ap12.hana.ondemand.com":
      return path.join("envs", `ap12.${space_name}.env`);
    default:
  }
};

/**
 * * dayjs DateTime
 * @param {*} format DateTime Format
 */
exports.getLocalDateTimeForTimezone = (format = "YYYY-MM-DD") => {
  dayjs.extend(utc);
  dayjs.extend(timezone);
  dayjs.tz.setDefault("Asia/Seoul");
  const datetimeFormat = dayjs(new Date()).format(format);
  return datetimeFormat;
};


/**
 * * Check Empty
 * @param {*} value
 */
exports.isEmpty = (value) => {
  if (value === "" || value == null || value === undefined || (value !== null && typeof value === "object" && !Object.keys(value).length)) {
    return true;
  }
  return false;
};

/**
 * * Deep Copy Function
 * ? JSON.parse(JSON.stringify(object))의 문제점
 * ? primitive values만 포함하는 객체 또는 배열에 대해 잘 작동하지만
 * ? 다른 객체 또는 배열에 대한 중첩 참조가 있는 모든 객체 또는 배열에 대해서는 
 * ? 실패하는 경우가 종종 발생하므로 별도의 Deep Copy함수를 구현하는걸 권장
 * @param {} inObject
 */
exports.deepCopyObject = (inObject) => {
  let outObject, value, key;

  if (typeof inObject !== "object" || inObject === null) {
    return inObject; // Return the value if inObject is not an object
  }

  // Create an array or object to hold the values
  outObject = Array.isArray(inObject) ? [] : {};

  for (key in inObject) {
    value = inObject[key];

    // Recursively (deep) copy for nested objects, including arrays
    outObject[key] = this.deepCopyObject(value);
  }

  return outObject;
};

/**
 * * Custom response Error
 * @param {Error} error
 */
exports.printError = (error) => {
  if (!(error instanceof Error)) return "Not Error Obejct";

  const countCRLF = error.stack.match(/\n/gi).length;
  const previousCRLFIdx = error.stack.indexOf("\n");
  const nextCRLFIdx = error.stack.indexOf("\n", previousCRLFIdx + 1);
  const charLength = nextCRLFIdx - previousCRLFIdx;

  const stackMessage = error.stack.substr(previousCRLFIdx + 1, charLength - 1).trim();

  const prettyError = {
    status: error.status,
    code: error.code,
    message: error.message,
    stack: stackMessage,
  };

  return prettyError;
};