const got = require('got');
const axios = require('axios');


/**
 * * 001_전자세금계산서 발행 (TAX_C001) - GOT Module (URL Params 사용)
 * TODO: HTTP 414 오류 발생
 * @param {*} request_data
 * @param {*} headers
 */
exports.procPostTaxInvoiceGotRequestParams = async ({
  request_data,
  headers
}) => {
  try {
    const { ...postTaxInvoiceReqeustData } = { ...request_data };

    //* 발행 요청 (POST)
    const options = {
      prefixUrl: process.env.WEBCASH_TAXINVOICE_ENDPOINT,
      responseType: 'json',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    };

    const requestUrl = `TaxGate?JSONData=${JSON.stringify(postTaxInvoiceReqeustData)}`; // Reqeust Params
    const response = await got.post(requestUrl, options).catch((error) => {
      console.log(error);
      return error;
    });
    if (response instanceof Error) throw response;

    return response.body;
  } catch (error) {
    return error;
  }
}

/**
 * * 002_전자세금계산서 발행 (TAX_C001) - GOT Module (RequestBody 사용)
 * TODO: HTTP 414 오류 발생
 * @param {*} request_data
 * @param {*} headers
 */
 exports.procPostTaxInvoiceGotRequestBody = async ({
  request_data,
  headers
}) => {
  try {
    const { ...postTaxInvoiceReqeustData } = { ...request_data };

    //* 발행 요청 (POST)
    const options = {
      prefixUrl: process.env.WEBCASH_TAXINVOICE_ENDPOINT,
      responseType: 'json',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      json: postTaxInvoiceReqeustData  // Reqeust Body
    };

    const requestUrl = `TaxGate`;
    const response = await got.post(requestUrl, options).catch((error) => {
      console.log(error);
      return error;
    });
    if (response instanceof Error) throw response;

    return response.body;
  } catch (error) {
    return error;
  }
}

/**
 * * 003_전자세금계산서 발행 (TAX_C001) - Axios Module (Request Params 사용)
 * TODO: HTTP 414 오류 발생
 * @param {*} request_data
 * @param {*} headers
 */
 exports.procPostTaxInvoiceAxiosRequestParams = async ({
  request_data,
  headers
}) => {
  try {
    const { ...postTaxInvoiceReqeustData } = { ...request_data };
    //* 발행 요청 (POST)
    const options = {
      url: `${process.env.WEBCASH_TAXINVOICE_ENDPOINT}/TaxGate`,
      method: 'POST',
      responseType: 'json',
      params: {
        JSONData: JSON.stringify(postTaxInvoiceReqeustData)
      },
      headers: { 
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    };

    const response = await axios(options).catch((error) => {
      console.log(error);
      return error;
    });
    if (response instanceof Error) throw response;

    return response.data;
  } catch (error) {
    return error;
  }
}

/**
 * * 004_전자세금계산서 발행 (TAX_C001) - Axios Module (Request Body 사용)
 * TODO: HTTP 414 오류 발생
 * @param {*} request_data
 * @param {*} headers
 */
 exports.procPostTaxInvoiceAxioisRequestBody = async ({
  request_data,
  headers
}) => {
  try {
    const { ...postTaxInvoiceReqeustData } = { ...request_data };

    //* 발행 요청 (POST)
    const options = {
      url: `${process.env.WEBCASH_TAXINVOICE_ENDPOINT}/TaxGate`,
      method: 'POST',
      responseType: 'json',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      json: postTaxInvoiceReqeustData  // Reqeust Body
    };

    const response = await axios(options).catch((error) => {
      console.log(error);
      return error;
    });
    if (response instanceof Error) throw response;

    return response.data;
  } catch (error) {
    return error;
  }
}