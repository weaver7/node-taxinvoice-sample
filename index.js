const express = require('express');
const app = express();
const bodyParser = require('body-parser');

require('dotenv').config({ path: './envs/lcl.env' });

const port = process.env.PORT || 5200;
const API_VERSION = 'v1';

app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);

app.use(
  bodyParser.json({
    limit: "50mb",
  })
);

const { taxInvoiceWebcashRouter } = require('./routes');

// Business
app.use(`/api/${API_VERSION}/taxInvoice`, taxInvoiceWebcashRouter);


if (process.env.NODE_ENV === "production") {
  app.listen(port, () => {
    console.log(`Server prod ${port}`);
  });
} else if (!module.parent) {
  app.listen(port, () => {
    console.log(`Server dev ${port}`);
  });
}

module.exports = app;